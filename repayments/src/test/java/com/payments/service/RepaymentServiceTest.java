package com.payments.service;

import static org.junit.Assert.assertTrue;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.payments.dto.LoanRequest;
import com.payments.dto.RePaymentPlan;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RepaymentServiceTest {

	@Autowired
	RepaymentService rePaymentservice;

	@Test
	public void testRepaymentServiceTest() throws ParseException {
		LoanRequest loanReq = new LoanRequest();
		loanReq.setDuration(24);
		loanReq.setLoanAmt((5000d));
		loanReq.setNominalRate((5.0));
		String testDate = "02-01-2022,13:00:14 PM";
		DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy,HH:mm:ss aaa");
		Date date = formatter.parse(testDate);

		loanReq.setStartDate(date);
		List<RePaymentPlan> paymentPlanList = rePaymentservice.getRepaymentSchedule(loanReq);
		assertTrue(paymentPlanList.size() == 24);
		RePaymentPlan repaymentResponse = paymentPlanList.get(0);
		assertTrue(repaymentResponse.getInitialOutStandingPrincipal() == 5000);
		assertTrue(repaymentResponse.getPrincipal() == 198.53);
		assertTrue(repaymentResponse.getRemainingOutStandingPrincipal() == 4801.47);
	}

}
