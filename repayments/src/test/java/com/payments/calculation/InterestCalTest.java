package com.payments.calculation;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.spy;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.jupiter.api.Test;


public class InterestCalTest {

	InterestCal interestCalculate = spy(InterestCal.class);;

	@Test
	public void calInterestperPeriod() {
		double loan = 5000d;
		double nominalRate = 5.0;
		loan = interestCalculate.calculateInterestPerPeriod(loan, nominalRate);
		//System.out.print(loan);
		loan = new BigDecimal(loan).setScale(2, RoundingMode.HALF_UP).doubleValue();
		assertTrue(loan == 20.83);
	}
}
