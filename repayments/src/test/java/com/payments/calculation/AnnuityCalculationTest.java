package com.payments.calculation;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.spy;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
class AnnuityCalculationTest {

	AnnuityCalculation annuityCalculation = spy(AnnuityCalculation.class); 
	
	@Test
	public void calAnnuity() {
		double loan = 5000d;
		int numofPeriods = 24;
		double interPerPeriod = (5.0 / 12);
		double annuityAmt = annuityCalculation.calculateAnnuity(loan, interPerPeriod, numofPeriods);
		annuityAmt = new BigDecimal(annuityAmt).setScale(2, RoundingMode.HALF_UP).doubleValue();
		assertTrue(annuityAmt == 219.36);
	}
}
