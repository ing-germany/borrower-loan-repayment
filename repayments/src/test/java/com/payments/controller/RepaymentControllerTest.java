package com.payments.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.payments.PaymentsApplication;
import com.payments.dto.LoanRequest;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PaymentsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RepaymentControllerTest {

	@LocalServerPort
	private int port;

	@Autowired
	RepaymentController controller;

	@Test
	void contextLoads() throws Exception {
		assertThat(controller).isNotNull();
	}

	@Test
	public void paymentScheduleTest() {
		LoanRequest loanReq = new LoanRequest();
		loanReq.setDuration(24);
		loanReq.setLoanAmt((5000d));
		loanReq.setNominalRate((5.0));
		loanReq.setStartDate(new Date());
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<LoanRequest> entity = new HttpEntity<LoanRequest>(loanReq, headers);
		ResponseEntity<List> response = restTemplate.exchange(createURLwithRandomPort("/generate-plan"),
				HttpMethod.POST, entity, List.class);
		HttpStatus status = response.getStatusCode();
		assertTrue(status.value() == 200);
		List body = response.getBody();
		assertTrue(body.size() == 24);

	}

	private String createURLwithRandomPort(String uri) {
		// TODO Auto-generated method stub
		return "http://localhost:" + port + uri;
	}

}
