package com.payments.exception;

public interface ExceptionConstants {

    String SERVER_ERROR = "SERVER ERROR FOUND";
	
	String INVALID_INPUTS = "Given inputs are invalid";
}

