package com.payments.exception;

import org.springframework.http.HttpStatus;

public class ErrorResponse extends RuntimeException {
	private String message;
	private HttpStatus code;

	public ErrorResponse(String message, HttpStatus statusCode) {
		super(message);
		this.message = message;
		this.code = statusCode;
	}
}